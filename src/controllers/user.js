const { validate } = require('joi');
const Joi = require('joi');
const { joi, validation } = require('joi-validation');
const sql = require("../config");
const logger = require('../helper/logger');


exports.getUser = async(req, res) => {
    try {
      sql.query("SELECT * FROM user",(err,rows,fields)=>{
        if(!err){
        res.json(rows);
      // if(args["test"] !== 1) console.log(rows);
    }
        else
        console.log(err);
      })
       // console.log("hello......")
       // res.status(200).json({success : "1", message: "Hello!"});
    } catch (error) {
      let data = {
        url : req.url,
        queryString : req.params,
        headers : req.headers,
        body : req.body,
        error : error
      }
      console.log("error       ", error)
      // await logger(error.toString());
      await logger(data);
      res.status(400).json({success : "0", error: "Bad request!"});
    }
};

exports.creatUser = async(req, res) => {
    try {
      const { body } = req; 
      const schema =({
        body: {
         type: "object",
         required: ["username","name","email","password"],
          properties: {
          username: { type: "string" },
            name:{type:"string"},
           email: { type: "string" ,index:{unique:true}},
         /*
            email: {
              type: String,
              validate: {
                validator: async function(email) {
                  const user = await this.constructor.findOne({ email });
                  if(user) {
                    if(this.id === user.id) {
                      return true;
                    }
                    return false;
                  }
                  return true;
                },
                message: props => 'The specified email address is already in use.'
              },
              required: [true, 'User email required']
            }
            // ...
          })*/
            password: { type: "string" }
          },
        },
        response: {
          200: { type: "string" },
        },
      });
      const v =new validate(body);
      const result=v.validate(body);
     // const result = schema.validate(body); 
     
      const { value, error } = result; 
      const valid = error == null; 
      if (!valid) { 
        res.status(422).json({ 
          message: error.details[0].message 
        }) 
      } else { 
        let data = {
          user_id: 123,
          company_id: 1, 
          role_id: 24,
          email: value.email
        }
       
        let {name,username,email, password} =req.body
        name =  JSON.stringify(value.name)
        username =  JSON.stringify(value.username)
        email =  JSON.stringify(value.email)
        password =  JSON.stringify(value.password)
        sql.query("INSERT INTO user SET ?", (err, res) => {
          //sql.query("INSERT INTO user values(?,?,?,?)", (err, res) => {
            if (err) {
              console.log("error: ", err);
              result(err, null);
              return;
            }
            console.log("created user: ");
           // result(null, { });
          });
      };
    } catch (error) {
      let data = {
        url : req.url,
        queryString : req.params,
        headers : req.headers,
        body : req.body,
        error : error
      }
      console.log("error       ", error)
      // await logger(error.toString());
    await logger(data);
     res.status(400).json({success : "0", error: "Bad request!"});
    }
  };

 exports.reg= (req,res) => {
  //const {name,email,password,username}=req.body
  let insertQuery = "INSERT INTO user (name,email,password,username) VALUES ('" + req.body.name + "', '" + req.body.email + "', '" + req.body.password +"','" + req.body.username + "');";

  const schema = Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string().email({minDomainSegments:2,tlds:{allow:['com','net','in']}}).required(),     
      password: Joi.string().required().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),//.min(8)
      username: Joi.string().optional().min(6),
      //user: Joi.string().required()
  })
  //console.log(schema.validate(req.body));

  if (schema.validate(req.body).error) {
      res.send(schema.validate(req.body).error.details);
  }

  else {
      sql.query(insertQuery, (err, field) => {
      if (err) {
        console.log("error: ", err);
        res.send(err);
        return;
      }
      res.send(schema.validate(req.body));
      console.log("user created");
    });
  }
}

exports.getOneUser = async(req, res) => {
  // log raw queries.. 
  // log=1
  // 
    sql.query("SELECT * FROM user WHERE id=?",[req.params.id],(err,rows,fields)=>{
        if(!err){
        res.send(rows);
       // console.log(rows);
      }
        else
        console.log(err);
    })
}

exports.delete=(req,res)=>{
  sql.query("DELETE FROM user WHERE id=?",[req.params.id],(err,rows,fields)=>{
      if(!err){
      res.send('deleted user');
  }
   else
      console.log(err);
  })
}
exports.update=(req,res)=>{
  const emp=req.body;
  let sql1 = 'UPDATE user SET ? WHERE id = ?';
  let id = req.params.id;
  sql.query(sql1, [emp, id], (err, response) => {
    if (err) throw err;
    res.json({message: 'Update success!'});
  })
}

