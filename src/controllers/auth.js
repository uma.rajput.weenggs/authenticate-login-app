const Joi = require("joi");
const tokenValidate = require("../core/tokenValidate");
// const User = require("../../model/userModel")
const sql = require("../config");
// const { date } = require('joi');
const logger = require("../helper/logger");

// login
/*
exports.login = async (req, res) => {
  try {
    const { body } = req;
    const schema = Joi.object().keys({
      email: Joi.string().required().messages({
        "any.required": `email/username is a required field`,
        "string.empty": `email/username cannot be an empty field`,
      }),
      password: Joi.string().required().messages({
        "any.required": `Password is a required field`,
        "any.empty": `Password cannot be an empty field`,
      }),
    });
    const result = schema.validate(body);
    const { value, error } = result;
    const valid = error == null;
    if (!valid) {
      res.status(422).json({
        message: error.details[0].message,
      });
    } else {
      let data = {
        user_id: 123,
        company_id: 1,
        role_id: 24,
        email: value.email,
      };

      let { email, password } = body;
      email = JSON.stringify(value.email);
      await sql.query(
        `SELECT * FROM user WHERE email = ${email} or username = ${email} and password =${password}`,
        async (err, value) => {
          if (err) {
            //  await logger(JSON.stringify(err));
            let demo = {
              user_id: 123,
              company_id: 1,
              err: err,
            };
            await logger(demo);
            res.status(400).json({ success: "0", message: "Failed to login" });
          } else if (value.length) {
            let token = tokenValidate.generate(data);
            let refreshToken = tokenValidate.generateRefreshToken(data);
            let currentDate = new Date();
            sql.query(
              "UPDATE user SET access_token = ?, refresh_token = ?, modified = ?",
              [token, refreshToken, currentDate]
            );

            delete value[0].password;
            delete value[0].access_token;
            delete value[0].refresh_token;
            res.status(200).json({
              success: "1",
              message: "Login successfully",
              access_token: token,
              refresh_token: refreshToken,
              data: value[0],
            });
            /*
            console.log({
              success: "1",
              message: "Login successfully",
              access_token: token,
              refresh_token: refreshToken,
              data: value[0],
            });*/
         /* } else {
            res.status(400).json({
              success: "0",
              message: "Email or password doesn't match. Please try again",
            });
          }
        }
      );
    }
  } catch (error) {
    let data = {
      url: req.url,
      queryString: req.params,
      headers: req.headers,
      body: req.body,
      error: error,
    };
    console.log("error       ", error);
    // await logger(error.toString());
    await logger(data);
    res.status(400).json({ success: "0", error: "Bad request!" });
  }
//};*/
exports.login = async (req, res) => {
  try {
    const { body } = req;
    const schema = Joi.object().keys({
      email: Joi.string().required().messages({
        "any.required": `email/username is a required field`,
        "string.empty": `email/username cannot be an empty field`,
      }),
      password: Joi.string().required().messages({
        "any.required": `Password is a required field`,
        "any.empty": `Password cannot be an empty field`,
      }),
    });
    const result = schema.validate(body);
    const { value, error } = result;
    const valid = error == null;
    if (!valid) {
      res.status(422).json({
        message: error.details[0].message,
      });
    } else {
      let data = {
        id: 1,
        //company_id: 1,
       // role_id: 24,
        email: value.email,
      };

      let { email, password } = body;
      email = JSON.stringify(value.email);
      await sql.query(
        `SELECT * FROM user WHERE email = ${email} or username = ${email} and password =${password}`,
        async (err, value) => {
          if (err) {
            //  await logger(JSON.stringify(err));
            let demo = {
              id: 1,
             // company_id: 1,
              err: err,
            };
            await logger(demo);
            res.status(400).json({ success: "0", message: "Failed to login" });
          } else if (value.length) {
            let token = tokenValidate.generate(data);
            let refreshToken = tokenValidate.generateRefreshToken(data);
            let currentDate = new Date();
            sql.query(
              "UPDATE user SET access_token = ?, refresh_token = ?, modified = ?",
              [token, refreshToken, currentDate]
            );

            delete value[0].password;
            delete value[0].access_token;
            delete value[0].refresh_token;
            res.status(200).json({
              success: "1",
              message: "Login successfully",
              access_token: token,
              refresh_token: refreshToken,
              data: value[0],
            });
            /*
            console.log({
              success: "1",
              message: "Login successfully",
              access_token: token,
              refresh_token: refreshToken,
              data: value[0],
            });*/
          } else {
            res.status(400).json({
              success: "0",
              message: "Email or password doesn't match. Please try again",
            });
          }
        }
      );
    }
  } catch (error) {
    let data = {
      url: req.url,
      queryString: req.params,
      headers: req.headers,
      body: req.body,
      error: error,
    };
    console.log("error       ", error);
    // await logger(error.toString());
    await logger(data);
    res.status(400).json({ success: "0", error: "Bad request!" });
  }
};

exports.refreshToken = async (req, res) => {
  try {
    const { body } = req;
    const schema = Joi.object().keys({
      refresh_token: Joi.string().required().messages({
        "any.required": `refresh_token is a required field`,
        "string.empty": `refresh_token cannot be an empty field`,
      }),
    });
    const result = schema.validate(body);
    const { value, error } = result;
    const valid = error == null;
    if (!valid) {
      res.status(422).json({
        message: error.details[0].message,
      });
    } else {
      let refresh_token = JSON.stringify(value.refresh_token);
      // console.log("refresh_token   ", refresh_token)
      await sql.query(
        `SELECT * FROM user WHERE refresh_token = ${refresh_token}`,
        (err, value) => {
          if (err) {
            console.log("error: ", err);
            res
              .status(400)
              .json({ success: "0", message: "Failed to refresh_token" });
          }
          if (value.length) {
            let data = {
              email: value[0].email,
            };
            let token = tokenValidate.generate(data);
            let currentDate = new Date();
            sql.query("UPDATE user SET access_token = ?, modified = ?", [
              token,
              currentDate,
            ]);
            res.status(200).json({ success: "1", access_token: token });
            console.log({ success: "1", access_token: token });
          } else {
            res
              .status(400)
              .json({ success: "0", message: "refresh_token is expire." });
          }
        }
      );
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({ success: "0", error: error });
  }
};
