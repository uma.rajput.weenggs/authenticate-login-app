const { request } = require("chai");
let chai = require("chai");
const assert = chai.assert;
const expect = require("chai").expect;
let chaiHttp = require("chai-http");
var should = chai.should();
chai.use(chaiHttp);
let server = require("../../index");

    describe(" users", () => {
      it("should GET users", (done) => {
        chai
          .request(server)
          .get("/users/getUsers")
          .end((err, res) => {
            res.should.have.status(200);
          //  res.should.be.json;
          //  res.body.should.be.a("array");
           // res.body[0].should.have.property("id");
           // res.body[0].should.have.property("name");
           // res.body[0].should.have.property("email");
           // res.body[0].should.have.property("password");
            done();
          });
      });

      // describe("registration", function () {
    it("should register", function (done) {
      chai
        .request(server)
        .post("/users/reg")
        .send({
          name:"uma rajput",
          email: "rajput@gmail.com",
          password: "12345678",
          username:"rajput"
        })
        .end((err, res) => {
          res.should.have.status(200);
         // console.log("successfully registerd");
         // res.body.should.be.a("object");
          done();
        });
    });
 // });

 // describe("/GET single user", () => {
    it("should GET one user", (done) => {
      chai
        .request(server)
        .get("/users/getUsers/50")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  //});
  

  //describe("delete", function () {
    it("should be deleted", function (done) {
      chai
        .request(server)
        .delete("/users/22")
        .end((err, res) => {
          res.should.have.status(200);
        //  console.log("successfully deleted");
         // res.body.should.be.a("object");
          done();
        });
    });
  //});


 
//describe("update", function () {
  it("should be updated", function (done) {
    chai
      .request(server)
      .put("/users/28")
      .send({
        name:"uma",
        email:"u@gmail.com"
      })
      .end((err, res) => {
        res.should.have.status(200);
     //   console.log("successfully updated");
       // res.body.should.be.a("object");
        done();
      });
  });
//});
});
   