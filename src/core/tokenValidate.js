var jwt = require('jsonwebtoken');
const { token } = require('morgan');
const sql = require("../config");

exports.generate = (req) => {
    let token = jwt.sign({
        data: req
      }, 'secret',  { expiresIn: '24h' },{ algorithm: 'RS256' });
      return token;
}

exports.generateRefreshToken = (req) => {
  let token = jwt.sign({
      data: req
    }, 'refreshSecret',  { expiresIn: '720h' },{ algorithm: 'RS256' });
    return token;
}

exports.verifyToken = async (req,res, next) => {
  try {
    let token = req.headers.authorization;
    token = token.split(/\s+/);
    jwt.verify(token[1], 'secret', async function (err, payload) {
      if (err) {
        // Not a valid token
        console.log('Error:         -----', err);
        res.status(401).json({success : "0", error: err});
      } else {
        let email =  JSON.stringify(payload.data.email);
        await sql.query(`SELECT * FROM user WHERE email = ${email} or username = ${email}`, async(err, value) => {
          if (err) {
            res.status(401).json({success: "0", message: "Unauthorized user"});
          }else if (value.length) {
            next();
          }});
        // Token successfully verified
      }
    });
  } catch (error) {
    res.status(400).json({success : "0", message: "Bad request!"});
  }
}
