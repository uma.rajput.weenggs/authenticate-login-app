const sql = require("../config");
// constructor
const User = function(user) {
    this.user_id = user.user_id;
    this.company_id = user.company_id;
    this.first_name = user.first_name;
    this.last_name = user.last_name;
    this.email = user.email;
    this.password = user.password;
    this.username = user.username;
    this.created_date = user.created_date;
    this.updated_date = user.updated_date;
};

User.create = (newUser, result) => {
    sql.query("INSERT INTO user SET ?", newUser, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
      console.log("created user: ", { id: res.insertId, ...newUser });
      result(null, { id: res.insertId, ...newUser });
    });
};

module.exports = User;