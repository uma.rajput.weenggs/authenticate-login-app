const mysql = require("mysql");
require('dotenv').config();
// Create a connection to the database
const dbConfig = {
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD || '',
  database: process.env.DB_NAME
}
const connection = mysql.createConnection(dbConfig);

// open the MySQL connection
connection.connect(error => {
  if (error) {
    console.log("errror ============", error)
  };
  console.log("Successfully connected to the database.");
});
module.exports = connection;