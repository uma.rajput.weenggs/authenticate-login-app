var express = require('express');
var router = express.Router();
// var schema = require('./schema')
// import validator from '../../helper/validation';
var auth = require('../controllers/auth');



/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/login', auth.login);
router.post("/refreshtoken", auth.refreshToken);



module.exports = router;
