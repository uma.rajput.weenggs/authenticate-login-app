const Joi = require('joi');

 module.exports = schema ={
    loginPayload: Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required()
    })
 }
