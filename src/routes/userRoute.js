var express = require('express');
var router = express.Router();

var schema = require('../routes/schema')
var user = require('../controllers/user')
/*
/**
 * @swagger
 * "/users/getUsers":
 * get:
 * summery: get users
 * description: get all users
 *  responses:
 * '200':
 *  content:
 *  application/json:
 *   schema:
 *  type: object
 *  properties:
 * id:
 * 
 *  description: id of user
 * example: 1
 * name:
 *  
 *  description: user name
 *  example: user
 */
 
//var User = require('../model/userModel')
var {verifyToken} = require('../core/tokenValidate')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('HELLO WORLD.....');
});
router.get('/getUsers', user.getUser);
//router.get('/getUsers', verifyToken, user.getUser);
router.get('/getUsers/:id', user.getOneUser);
router.delete('/:id', user.delete);
router.put('/:id', user.update);


router.post('/reg',user.reg);
router.post('/creatUsers',user.creatUser);

module.exports = router;
