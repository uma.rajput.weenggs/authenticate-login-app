const BadRequest = 0;

// module.export= ValidationSource ={
//     BODY : `body`,
//     HEADER : `headers`,
//     QUERY: `query`,
//     PARAM : `params`
// }




var Validationsource;
(function (Validationsource) {
    Validationsource["BODY"] = "body";
    Validationsource["HEADER"] = "header";
})(Validationsource = exports.Validationsource || (exports.Validationsource = {}));

// export default (schema, source = Validationsource.BODY) => (req, res, next) => {
//     try {
//         const { error } = schema.validate(req[source]);
//         if (!error)
//             return next();
//         const { details } = error;
//         const message = details.map((i) => i.message.replace(/['"]+/g, '')).join(',');
//         next(new BadRequest(message));
//     }
//     catch (error) {
//         next(error);
//     }
// };
