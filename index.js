
var createError = require('http-errors');
var express = require('express');
const cors = require("cors");
var path = require('path');
var cookieParser = require('cookie-parser');
require('dotenv').config();
const logger = require('./src/helper/logger');
const sql = require("./src/config.js");
var app = express();

const swaggerUI=require('swagger-ui-express');
/*
//const swaggerJsDoc=require('swagger-jsdoc');
//const swaggerOption=require('./swagger')
//const jsDoc=swaggerJsDoc(swaggerOption)
//const swaggerDocs=require('./swagger.json')*/
const YAML = require('yamljs');
const swaggerDocs = YAML.load('./swagger.yaml');
app.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));

//var indexRouter = require('./src/routes/index');
var usersRouter = require('./src/routes/userRoute');
var authRouter = require('./src/routes/authRoute');//
app.use('/users', usersRouter);
app.use('/', authRouter);

var corsOptions = {
  origin: "http://localhost:8081"
};
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors(corsOptions));
// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//access log
app.use(function(req, res, next) {
 // console.log("access log :", req.body)
  let data = {
    url: req.url,
    header: req.headers,
    params: req.params,
    query: req.query,
    body: req.body,
  }
   logger(data);
  next()
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  logger(req);
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// module.exports = app;
const PORT = process.env.PORT || 8080;
module.exports=app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
//module.exports=app;